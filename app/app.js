/**
 * Created by admin on 5/7/15.
 */

angular.module('demoGraphApp', ['ui.router', 'graph-component', 'editor-component'])

.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider

            .state('home', {
                url: '/',
                controller: 'ControllerWithGraph',
                templateUrl: 'components/graph.html'
            });

    }]);