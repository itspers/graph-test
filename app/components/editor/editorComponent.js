angular.module('editor-component',[])


    .directive('userEditor', ['$window', '$parse', function($window, $parse) {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '='
            },
            require: 'ngModel',
            templateUrl: 'components/editor/editorComponent.html',
            link: function(scope, elem, attrs) {


                scope.groups = [1,2,3,4,5];

                scope.selectUser = function(user){
                    scope.selectedIndex = scope.ngModel.indexOf(user);
                    scope.selectedUser = angular.copy(user);
                };

                scope.toggleChild = function(id){
                   if (scope.selectedUser.children.indexOf(id) >= 0) {
                       scope.selectedUser.children.splice(scope.selectedUser.children.indexOf(id),1);
                   } else {
                       scope.selectedUser.children.push(id);
                   }
                };

                scope.resetUser = function(){
                    scope.selectedUser = {name: 'New user', children:[], level: 1};
                    scope.selectedIndex = -1;
                };

                scope.resetUser();

                scope.saveUser = function(){

                    //fix bad children
                    var goodUsers = [];
                    for (var k = scope.ngModel.length-1; k>=0; k--){
                        if (scope.ngModel[k].level == scope.selectedUser.level +1 && scope.selectedUser.children.indexOf(scope.ngModel[k].id) >=0) {
                            goodUsers.push(scope.ngModel[k].id);
                        }
                    }
                    scope.selectedUser.children = goodUsers;




                    if (scope.selectedUser.id) {
                        scope.ngModel[scope.selectedIndex] = angular.copy(scope.selectedUser);
                    } else {
                        scope.selectedUser.id = Math.floor(Date.now() / 1000);
                        var user = angular.copy(scope.selectedUser);
                        scope.ngModel.push(user);
                        scope.selectUser(user);
                    }
                };

                scope.removeUser = function(){
                    scope.ngModel.splice(scope.selectedIndex, 1);
                    scope.resetUser();
                };
            }
        };
    }]);

