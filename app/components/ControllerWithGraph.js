/**
 * Created by admin on 5/7/15.
 */
angular.module('demoGraphApp')

    .controller('ControllerWithGraph', ['$scope', function($scope) {

        $scope.users = [
            {
                id: 1,
                name: 'Owner',
                level: 1,
                children: [2,3]
            },
            {
                id: 2,
                name: 'Top Manager 1',
                level: 2,
                children: [4,5]
            },
            {
                id: 3,
                name: 'Top Manager 2',
                level: 2,
                children: []
            },
            {
                id: 4,
                name: 'Manager',
                level: 3,
                children: [7,6]
            },
            {
                id: 5,
                name: 'Team Lead',
                level: 3,
                children: [8,7]
            },
            {
                id: 6,
                name: 'QA 1',
                level: 4,
                children: [9]
            },
            {
                id: 7,
                name: 'Developer 1',
                level: 4,
                children: []
            },
            {
                id: 8,
                name: 'Developer 2',
                level: 4,
                children: []
            },
            {
                id: 9,
                name: 'Cleaning manager',
                level: 5,
                children: []
            }
        ];




    }]);