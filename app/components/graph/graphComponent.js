angular.module('graph-component',[])


    .directive('usersGraph', ['$window', function($window) {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '='
            },
            require: 'ngModel',
            templateUrl: 'components/graph/graphComponent.html',
            link: function(scope, elem, attrs, ngModel) {

                //vars
                var nodes = [];
                var links = [];
                var maxlevels = 0;
                var d3 = $window.d3;
                var rawSvg = elem.find('svg');
                var svg = d3.select(rawSvg[0]);
                var w, h, force, link, node;
                var fill = d3.scale.category20();

                //get fluid svg size
                function calcSizes(){
                    w = svg.node().getBoundingClientRect().width;
                    h = svg.node().getBoundingClientRect().height;
                }
                calcSizes();


                //build chart
                function rebuildGraph(){

                    //clean up on model changed
                    svg.selectAll("*").remove();

                    //prepare nodes and links
                    prepareChartData(scope.ngModel);

                    //init force layout
                    force = d3.layout.force()
                        .charge(-3000)
                        .distance(80)
                        .friction(0.5)
                        .gravity(0.5)
                        .nodes(nodes)
                        .links(links)
                        .size([w, h])
                        .start();


                    //arrow
                    svg.append("svg:defs").selectAll("marker")
                        .data(["end"])
                        .enter().append("svg:marker")
                        .attr("id", String)
                        .attr("viewBox", "0 -3 6 6")
                        .attr("refX", 10)
                        .attr("refY", 0)
                        .attr("fill", '#999999')
                        .attr("markerWidth", 3)
                        .attr("markerHeight", 3)
                        .attr("orient", "auto")
                        .append("svg:path")
                        .attr("d", "M0,-3L6,0L0,3");


                    //lines
                    link = svg.selectAll('line.link')
                        .data(links)
                        .enter().append('svg:line')
                        .attr("class", "link")
                        .style('stroke-width', 3)
                        .attr("x1", function(d) { return d.source.x; })
                        .attr("y1", function(d) { return d.source.y; })
                        .attr("x2", function(d) { return d.target.x; })
                        .attr("y2", function(d) { return d.target.y; })
                        .attr("marker-end", "url(#end)");

                    //nodes
                    node = svg.selectAll("circle.node")
                        .data(nodes)
                        .enter().append("g")
                        .attr("transform", function(d, i) {
                            return "translate(" + d.x + "," + d.y + ")";
                        })
                        .attr("class", "node")
                        .call(force.drag);

                    node.append("svg:circle")
                        .attr("r", 8)
                        .style("fill", function(d) { return fill(d.group); });

                    node.append("text")
                        .attr("x", 12)
                        .attr("fill","black")
                        .attr("dy", ".25em")
                        .text(function(d) { return d.name; });

                    //force layout calculate
                    force.on("tick", function(e) {


                        //fix Y coord by user level
                        node.each(function(d) {
                            d.y += ((d.group - 1) * (h / maxlevels - 20) - d.y) + 20;
                        });


                        //connect points
                        link.attr("x1", function(d) { return d.source.x; })
                            .attr("y1", function(d) { return d.source.y; })
                            .attr("x2", function(d) { return d.target.x; })
                            .attr("y2", function(d) { return d.target.y; });


                        //move circles
                        node.attr("transform", function(d, i) {
                            return "translate(" + d.x + "," + d.y + ")";
                        });

                    });



                }

                //redraw on window resize
                function redrawOnResize(){
                    calcSizes();
                    force.size([w, h]).resume();
                }
                $window.addEventListener('resize', function(event){
                    redrawOnResize();
                });


                //watch model changes
                scope.$watch(
                    function(){
                        return ngModel.$modelValue;
                    }, function(newValue, oldValue){
                        rebuildGraph();
                    }, true);


                //explode users array to nodes and lines
                function prepareChartData(rawUsers){
                    nodes = [];
                    links = [];
                    maxlevels = 0;
                    var count = rawUsers.length;

                    //get index of user by id
                    function findUserNumberById(id){
                        for(var i = 0; i < count; i++){
                            if (rawUsers[i].id == id) {
                                return i;
                            }
                        }
                        return false;
                    }

                    //push to arrays
                    for(var i = 0; i < count; i++){
                        nodes.push({name: rawUsers[i].name, group: rawUsers[i].level});
                        if (rawUsers[i].level > maxlevels) maxlevels = rawUsers[i].level;
                        for(var k = rawUsers[i].children.length -1; k>=0; k--){
                            links.push({source: i, target: findUserNumberById(rawUsers[i].children[k])});
                        }
                    }
                }



            }
        };
    }]);
